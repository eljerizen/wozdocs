$(function() {
  $('#home').on('click', function () {
    $('#navbar > li').removeClass('active');
    $('.j-container').html('<center><br><br><h1 class="text-muted">WoZ Documentation</h1><p class="text-muted">Develop & maintain by RGTC zendesk developer</p></center>');
  })

  $('#add-new-tab').on('click', function () {
    template.createModal({
      size: ' large ',
      container: '.j-modal-container',
      header: '<span class = "fa fa-edit"> Add Tabs</span>',
      body: '<div class="col-md-12">' +
              '<div class="form-group">' +
                '<label for="tab-name">Tab name:</label>' +
                '<input type="text" class="form-control" id="tab-name" placeholder="Enter tab name" name="tab-name">' +
              '</div>' +
              '<div class="form-group">' +
                '<label for="variable">Variable:</label>' +
                '<input type="text" class="form-control" id="variable" placeholder="Enter variable name with no space" name="variable">' +
              '</div>' +      
              '</div>',
      footer: '<div class = "row"><div class = "col-md-12"><button type = "button" id = "btn-post" class = "btn btn-sm btn-primary pull-right"> Post </button> <b class = "pull-right">&nbsp; &nbsp; ' +
          '<button id = "btn-cancel" class = "btn-modal-close btn btn-sm btn-default pull-right"> Cancel </button></div></div>',
      action: function () {
          $('#btn-post').on('click', function () {
              template.saveData('new_tab', {
                [$('#variable').val()]: {
                    "name": $('#variable').val(),
                    "data": ''
                  }
              }) 
              $('#btn-cancel').trigger('click');                
          });
      }
    });
  });

  template.initTab('development_setup');
  template.initTab('deployment');
  template.initTab('repository_management');

});