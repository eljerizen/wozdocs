(function () {
  
  var m_firebaseConfig = {
    apiKey: "AIzaSyDiMER1hDABDFBsQis8ivRRfd4sKbyhNnQ",
    authDomain: "firezen-2e894.firebaseapp.com",
    databaseURL: "https://firezen-2e894.firebaseio.com",
    projectId: "firezen-2e894",
    storageBucket: "firezen-2e894.appspot.com",
    messagingSenderId: "546084231513",
    appId: "1:546084231513:web:2febb53fec114986631448",
    measurementId: "G-R30T863HHK"
};
firebase.initializeApp(m_firebaseConfig);
  this.m_database = firebase.database();
  this.template = {
    createNotif: function (data) {
      var icon = '';
      if (data.status == "success") {
        icon = "fa-check";
      } else if (data.status == "error") {
        icon = "fa-times";
      } else {
        icon = "fa-warning";
      }
      var notif = '<div class="notif">' +
        '<strong class = "text-muted"><i class="fa ' + icon + '"></i>' + data.msg + '</strong>' +
        '<span class="notif-close fa fa-times pull-right"></span>' +
        '</div>';
      if (data.append) {
        $(data.container).append(notif);
      } else {
        $(data.container).html(notif);
      }
      $('.notif-close').click(function () {
        $(this).closest('.notif').hide();
      });
    },
    createModal: function (data) {
      var modal = '<div class="j-modal-bg">' +
        '<form id="' + data.form_id + '" method="POST" action="" class="form" enctype="multipart/form-data">' +
        '<div class="j-modal-box ' + data.size + '">' +
        '<span class="btn-modal-close fa fa-times modal-close"></span>' +
        '<div class="j-modal-header">' +
        data.header +
        '</div>' +
        '<div class="j-modal-body">' +
        data.body +
        '</div>' +
        '<div class="j-modal-footer">' +
        data.footer +
        '</div>' +
        '</div>' +
        '</form>' +
        '</div>';
      $(data.container).html(modal);
      $('body').addClass('no-scroll');

      $('.btn-modal-close').on('click', function () {
        $(data.container).html('');
        $('body').removeClass('no-scroll');
      });
      data.action();
    },

    saveData: function (loc, data) {
      m_database.ref(loc).set(data);
    },
    onView: function(loc, callback){        
      m_database.ref(loc).on('value', function(snapshot){
          callback(snapshot.val());
      })
    },
    initTab: function(tab){
      var display = ''; 
      m_database.ref(tab).once('value').then(function(snapshot){
          display = snapshot.val().data;
          $('#data-container').html(display);
      })
      $('#'+ tab).on('click', function () {
          $('#navbar > li').removeClass('active');
          $(this).addClass('active');     
          var development = '<div class = "col-md-12"><div class = "container"><button id = "edit-data" class = "btn btn-default btn-sm pull-right">Edit</button></div><hr/></div>';
          development += '<div class = "col-md-12"><div id = "data-container" class = "container"><center>Loading...</center></div></div>';
          $('.j-container').html(development);
          template.onView(tab, function(response){    
              console.log(response)       
              display = response.data;  
              $('#data-container').html(display);
              $('#edit-data').on('click', function () {                           
                  template.createModal({
                      size: ' large ',
                      container: '.j-modal-container',
                      header: '<span class = "fa fa-edit"> Create Docs </span>',
                      body: ' <textarea id="editor" name="editor" style ="height: 100%; width: 100%">' + display + '</textarea>',
                      footer: '<div class = "row"><div class = "col-md-12"><button type = "button" id = "btn-post" class = "btn btn-sm btn-primary pull-right"> Post </button> <b class = "pull-right">&nbsp; &nbsp; ' +
                          '<button id = "btn-cancel" class = "btn-modal-close btn btn-sm btn-default pull-right"> Cancel </button></div></div>',
                      action: function () {
                          var editor = CKEDITOR.replace('editor');
                          $('#btn-post').on('click', function () {
                              var data = editor.getData();
                              template.saveData(tab + '/data', data) 
                              $('#btn-cancel').trigger('click');                
                          });
                      }
                  });
              });
          });
      });
    }
    
  }
})();