(function () {
    var sample_tree = '<ul class = "tree">' +
        '<li><span class="carets">cmn</span>' +
        '<ul class="nested">' +
        '<li><span class="carets">cmn-cache.js</span>' +
        '<ul class="nested">' +
        '<li>multiTab()</li>' +
        '<li>user()</li>' +
        '</ul>' +
        '</li>' +
        '<li><span class="carets">cmn-collision.js</span>' +
        '<ul class="nested">' +
        '<li>createRecord()</li>' +
        '<li>updateRecord()</li>' +
        '<li>deleteRecord()</li>' +
        '</ul>' +
        '</li> ' +
        '</ul>' +
        '</li>' +
        '<li><span class="carets">ctrl</span>' +
        '<ul class="nested">' +
        '<li><span class="carets">A001.js</span>' +
        '<ul class="nested">' +
        '<li>initializeModule()</li>' +
        '<li>autoSynch()</li>' +
        '</ul>' +
        '</li>' +
        '<li><span class="carets">B001.js</span>' +
        '<ul class="nested">' +
        '<li>initializeModule()</li>' +
        '<li>initializeOnClick()</li>' +
        '<li>initializeOnChange()</li>' +
        '</ul>' +
        '</li> ' +
        '</ul>' +
        '</li>' +
        '</ul>';

    var functions_container = '<div class="treeView">' + sample_tree + '</div>' +
        '<div class="description">' +
        '<div id = "ctrl-container">' +
            '<div class = "col-md-12">' +
                '<div class = "col-md-4">' +
                    '<b id = "nodes">cmn > cmn-cache.js > multiTab()</b>' +
                '</div>' +
                '<div class = "col-md-4">' +
                    '<div class="form-inline">' +
                        'Search: <input type="text" class="form-control" id="search key" placeholder="Search here">' +
                    '</div>' +
                '</div>' +
                '<div class = "col-md-4">' +
                    '<button id ="edit-data" class = "btn btn-default btn-sm pull-right">Edit</button>' +
                '</div>' +
            '</div>' +
        '</div>' +
        '<div id = "function-data-container" class = "text-muted">' + 
            '<div class = "col-md-12"><hr/>' +
                '<h2><b>.multiTab()</b><h2>' +
                '<div>' +
                    '<div><b> Purpose: </b><span> use in opening multiple ticket</span></div>' +
                    '<div><b> Methods: </b><ul><li> .pushData()</li> <li> .getTabs()</li> <li> .createCtxMenuItem()</li></ul></div>' +
                '</div>' +
            '</div>' +
        '</div>' +
        '</div>';
    $('#functions').on('click', function () {
        $('#navbar > li').removeClass('active');
        $(this).addClass('active');
        $('.j-container').html(functions_container);
        var toggler = document.getElementsByClassName("carets");
        var i;
        for (i = 0; i < toggler.length; i++) {
            toggler[i].addEventListener("click", function () {
                this.parentElement.querySelector(".nested").classList.toggle("active");
                this.classList.toggle("carets-down");
            });
        }

                $('#edit-data').on('click', function () {                           
                    template.createModal({
                        size: ' large ',
                        container: '.j-modal-container',
                        header: '<span class = "fa fa-edit"> Create Docs </span>',
                        body: ' <textarea id="editor" name="editor" style ="height: 100%; width: 100%"></textarea>',
                        footer: '<div class = "row"><div class = "col-md-12"><button type = "button" id = "btn-post" class = "btn btn-sm btn-primary pull-right"> Post </button> <b class = "pull-right">&nbsp; &nbsp; ' +
                            '<button id = "btn-cancel" class = "btn-modal-close btn btn-sm btn-default pull-right"> Cancel </button></div></div>',
                        action: function () {
                            
                          var editor = CKEDITOR.replace('editor');
                            $('#btn-post').on('click', function () {
                                var data = editor.getData(); 
                                $('#btn-cancel').trigger('click');                
                            });
                        }
                    });
                });
        

    });
})();
